//
//  ConfirmRentCarViewController.swift
//  Adry_RentCar
//
//  Created by MacPro on 11/26/19.
//  Copyright © 2019 MacPro. All rights reserved.
//

import UIKit

class ConfirmRentCarViewController: UIViewController {
    
    var text:String = ""
    var rentModel: RentModel?

    @IBOutlet weak var lblModelo: UILabel?
    @IBOutlet weak var lblMarca: UILabel?
    @IBOutlet weak var lblFechaInicio: UILabel?
    @IBOutlet weak var lblFechaFin: UILabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Confirmación de renta"
        configOulets()
    }

    func configOulets (){
        lblMarca?.text = rentModel?.car?.marca
        lblModelo?.text = rentModel?.car?.modelo
        lblFechaInicio?.text = rentModel?.fechaInicio
        lblFechaFin?.text = rentModel?.fechaFin
    }
}
