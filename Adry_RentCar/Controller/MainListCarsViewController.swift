//
//  MainListCarsViewController.swift
//  Adry_RentCar
//
//  Created by MacPro on 11/26/19.
//  Copyright © 2019 MacPro. All rights reserved.
//

import UIKit

class MainListCarsViewController: UIViewController {
    
    let net = NetworkRequestManager()
    let detailCarView = DetailCarViewController()

    @IBOutlet weak var tableListCars: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getDataNetwork()
        configViewAndTable()
    }
    
    func configViewAndTable(){
        self.title = "Renta de carros Nova"
        tableListCars.delegate = self
        tableListCars.dataSource = self
        tableListCars.register(UINib(nibName: "CarInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "carInfoCell")
    }
    
    func getDataNetwork(){
        net.getDataCar { results in
           switch results {
           case .success :
            self.tableListCars.reloadData()
           case .failure(let error):
        print(error.localizedDescription)
           }
        }
   }
}

// MARK: - MainViewController Extension - Table view data source and delegate

extension MainListCarsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.net.modelCar.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "carInfoCell", for: indexPath) as! CarInfoTableViewCell
        let dict = self.net.modelCar[indexPath.row]
        cell.lblMarca.text = dict.marca
        cell.lblModelo.text = dict.modelo
        cell.lblPrecioRenta.text = dict.precioRenta
        return cell
    }
}

extension MainListCarsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = self.net.modelCar[indexPath.item]
        detailCarView.car = item
        self.navigationController?.pushViewController(detailCarView, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
