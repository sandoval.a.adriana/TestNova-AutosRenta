//
//  NetworkRequestManager.swift
//  Adry_RentCar
//
//  Created by MacPro on 11/24/19.
//  Copyright © 2019 MacPro. All rights reserved.
//

import Foundation

class NetworkRequestManager{
    
    var modelCar = [Car]()
    
    func getDataCar (completion: @escaping (Result<[Car], Error>) -> Void){
        guard let url = URL(string: "http://interviewtest-luis666.cfapps.io/vehicles") else {return}
        URLSession.shared.dataTask(with: url) { data, _, error in
            if let error = error {
                DispatchQueue.main.async {
                    completion(.failure(error))
                }
                return
            }
            
            guard let data = data else { return }
            do {
                let decoder = JSONDecoder()
                let modelCar = try decoder.decode([Car].self, from: data)

                for dic in modelCar{
                    self.modelCar.append(dic)
                }
            } catch let errorDecode {
                print("Error Decode: ", errorDecode)
            }

            let results = self.modelCar
            DispatchQueue.main.async {
                completion(.success(results))
            }
        }.resume()
    }
}
